var gulp = require('gulp'),
    borwserSync = require('browser-sync').create(),
    reload = borwserSync.reload,
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    spriter = require('gulp-css-spriter'),
    fs= require('fs'),
    minimist = require('minimist');

var src = {
    scss : './sass/**/*.scss',
    html : './**/*.html',
    css_spriter: 'dist/css/engineer-join.css'
}

gulp.task('sass',function(){
    return gulp.src( src.scss )
               .pipe( sass() )
               .pipe( gulp.dest( './dist/css/' ) );
});

gulp.task('sprite', function() {
        //var timestamp = +new Date();
        //需要自动合并雪碧图的样式文件
        return gulp.src( src.css_spriter )
            .pipe(spriter({
                // 生成的spriter的位置
                'spriteSheet': '../kongfu/project/static/sprite/sprite-'+'engineer-join'+'.png',
                // 生成样式文件图片引用地址的路径
                // 如下将生产：backgound:url(../images/sprite20324232.png)
                'pathToSpriteSheetFromCSS': '../sprite/sprite-'+'engineer-join'+'.png'
                // 'spriteSheet': './dist/images/sprite'+timestamp+'.png',
                // 'pathToSpriteSheetFromCSS': '../images/sprite'+timestamp+'.png'
            }))
            //产出路径
            .pipe(gulp.dest('../kongfu/project/static/new-css/'));
});


gulp.task('browser-sync',['sass','sprite'],function(){
    borwserSync.init({
        server:{
            baseDir:['./','../']
        }
    });
    gulp.watch(src.scss,['sass']);
    gulp.watch(src.css_spriter,['sprite']);
    gulp.watch(src.html).on('change',reload);
});


gulp.task('default', ['browser-sync']);


